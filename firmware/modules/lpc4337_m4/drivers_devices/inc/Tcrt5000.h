/* Copyright 2020,
 *
 * Joaquín Horacio Monti:
 * joaquin.monti97@gmail.com
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */



/** @brief Header of the drive for a IR sensor in EDU-CIAA NXP
 **
 ** This is a driver for IR sensor "Tcrt5000"
 **
 **/


/*==================[inclusions]=============================================*/
#include "bool.h"
#include "gpio.h"
#include <stdint.h>

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/
/** @brief Initialization function to port in the EDU-CIAA BOARD
 * This function initialize the port of sensor present in the EDU-CIAA board
 * @return true if no error
 */
bool Tcrt5000Init(gpio_t dout);

/** @brief Function to read the state of sensor pin
 * The function returns a bool, depending the state of pin
 * @param[in] dout Port for a sensor pin
 * @return TRUE if the pin is <hight> and FALSE if the pin is <low>
 */
bool Tcrt5000State(void);

/** @brief Deinitialization function to port in the EDU-CIAA BOARD
 * This function deinitialize the port of sensor present in the EDU-CIAA board
 * @return true if no error
 */
bool Tcrt5000Deinit(gpio_t dout);

/*==================[end of file]============================================*/


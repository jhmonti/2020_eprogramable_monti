/*! @mainpage Procesamiento Digital
 *
 * \section genDesc General Description
 *
 * This application show a ECG signal and the result of digital filter in "Serial Osciloscope" program.\n
 *  Use DAC for convert a data of signal to analog data, and ADC for convert this analog data to digital data. \n
 *  The signal data converted is filtered with a low-pass filter algorithm. \n
 *  Finally, it sends them through serial port to the "Serial Osciloscope" program. \n
 *
 * The next video describes how the program works.
 *
 * <a href=" AGREGAR ">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * Connect CH1 and DAC through a potentiometer
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 09/10/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Joaquín H. Monti
 *
 */

#ifndef _TEMPLATE_H
#define _TEMPLATE_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */


/*! @mainpage Prueba Sensor
 *
 * \section genDesc General Description
 *
 * This application is a test for Tcrt5000 sensor which is utilized in contador objetos´s proyects
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 18/09/2020 | Document creation		                         |
 *
 * @author Joaquín H. Monti
 *
 */

#ifndef _BLINKING_SWITCH_H
#define _BLINKING_SWITCH_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */


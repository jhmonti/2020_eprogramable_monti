/*! @mainpage Prueba Sensor
 *
 * \section genDesc General Description
 *
 * This application is a test for Tcrt5000 sensor which is utilized in contador objetos´s proyects
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 18/09/2020 | Document creation		                         |
 *
 * @author Joaquín H. Monti
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/prueba_sensor.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "Tcrt5000.h"

/*==================[macros and definitions]=================================*/
#define COUNT_DELAY 3000000
#define NUM_TOGGLE 3
/*==================[internal data definition]===============================*/

void Delay(void)
{
	uint32_t i;

	for(i=COUNT_DELAY; i!=0; i--)
	{
		   asm  ("nop");
	}
}

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	bool pin_state;
	uint8_t i;
	SystemClockInit();
	LedsInit();
	Tcrt5000Init(T_COL0);
    while(1)
    {
    	pin_state = Tcrt5000State();
    	switch(pin_state)
    	{
    		case true:
    			for(i=0;i<NUM_TOGGLE;i++){
    				LedOn(LED_1);
    				Delay();
    				LedOff(LED_1);
    				Delay();
    			}
    		break;
			case false:
			break;
    	}
	}

    
}

/*==================[end of file]============================================*/


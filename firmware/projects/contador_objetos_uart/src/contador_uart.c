/*! @mainpage Contador de Objetos con Comunicación en Serie
 *
 * \section genDesc General Description
 *
 * This application counts objects with Tcrt5000 sensor and shows it in a terminal program for serial communication "Hterm"
 *
 * \section hardConn Hardware Connection
 *
 * |   Tcrt5000  	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	 DOUT	 	|  GPIO_T_COL0	|
 * | 	 +5V	 	|  	  +5V		|
 * | 	 GND 	 	|  	  GND		|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 02/10/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Joaquín H. Monti
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/contador_uart.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "Tcrt5000.h"
#include "bool.h"
#include "timer.h"
#include "uart.h"

/*==================[macros and definitions]=================================*/
#define BAUD_RATE 		115200 	//velocidad de transmicion de datos en serie
#define PERIODO_TIMER  	1000 //para que se transmita los datos cada 1[s]
/** @fn void doTimer(void);
 * @brief Function to use with a timer\n
 * This function add a count and was make to use with a timer system.
 * @param[in] void
 * @return void
 */
void doTimer(void);
/** @fn void doSerial(void);
 * @brief Function to use in serial communication\n
 * This function is used in received of data
 * @param[in] void
 * @return void
 */
void doSerial(void);

/*==================[internal data definition]===============================*/

/* banderas para decidir qué hacer en while(1)*/
bool sw1 = false;
bool sw2 = false;

uint16_t cuenta = 0; // contador de 16bits porque puede contar hasta 999 (por la limitante de num de digitos ASCII)
uint16_t cuenta_hold = 0;
/*==================[internal functions declaration]=========================*/

void doTimer(void) //función de cuenta para trabajar con Timer
{

	if(sw2 == false){ // el hold está inactivo, por lo que tengo que mostrar el valor del contador
		UartSendString(SERIAL_PORT_PC, UartItoa(cuenta, 10));
		UartSendString(SERIAL_PORT_PC, " lineas\r\n");
		cuenta_hold = cuenta; //guardo el último valor de cuenta que se mostró por si se acciona el HOLD
	}
	else{ // cuando se acciona el HOLD, sigo mandando el último valor de cuenta
		UartSendString(SERIAL_PORT_PC, UartItoa(cuenta_hold, 10));
		UartSendString(SERIAL_PORT_PC, " lineas\r\n");
	}
}
void doSerial(void)
{
	uint8_t dato;
	UartReadByte(SERIAL_PORT_PC, &dato);
	switch (dato){
		case 'O':
			sw1 = !sw1;
			break;
		case 'H':
			sw2 = !sw2;
			break;
		case '0':
			cuenta = 0;
			break;
	}
}
/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){
	/*Apago los leds porque me molestan que esten prendidos*/
	LedsInit();
	LedsOffAll();
	/*variables para los drivers*/
	timer_config timer_init = {TIMER_A,PERIODO_TIMER,doTimer};
	serial_config port = {SERIAL_PORT_PC,BAUD_RATE, doSerial};
	/*Inicializa todos los dispositivos y periféricos */
	SystemClockInit();
	Tcrt5000Init(GPIO_T_COL0);
	TimerInit(&timer_init);
	UartInit(&port);
	/* Inicio el timer*/
	TimerStart(TIMER_A);

	/*variables y banderas para el programa*/
	bool sensor;
	bool sensor_anterior;

	while(1)
	{
		if(sw1 == true){ //se tiene que contar

			sensor = Tcrt5000State(); // mido el valor actual del sensor

			if(sensor==false && sensor_anterior==true){ //detecta flanco descendente, debo aumentar la cuenta
				if(cuenta < 1000){ // como cuenta es una variable de 16bits se puede contar de 0 a 999 con 3 digitos
					cuenta ++;
				}
				else{
					cuenta = 0;
				}
			}
			sensor_anterior = sensor; //valor del sensor actual que será el anterior en la siguiente vuelta
		}
	}
    
	return 0;
}

/*==================[end of file]============================================*/


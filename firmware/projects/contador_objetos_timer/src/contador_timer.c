/*! @mainpage Contador de Objetos
 *
 * \section genDesc General Description
 *
 * This application count objects with Tcrt5000 sensor and show it in natural binary code through Leds
 *
 * \section hardConn Hardware Connection
 *
 * |   Tcrt5000  	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	 DOUT	 	|  GPIO_T_COL0	|
 * | 	 +5V	 	|  	  +5V		|
 * | 	 GND 	 	|  	  GND		|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 25/09/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Joaquín H. Monti
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/contador_timer.h"       /* <= own header */

#include "systemclock.h"
#include "led.h"
#include "Tcrt5000.h"
#include "switch.h"
#include "bool.h"
#include "timer.h"

/*==================[macros and definitions]=================================*/
#define L3  1<<0   //0000 0001
#define L2  1<<1   //0000 0010
#define L1  1<<2   //0000 0100
#define Lrgb  1<<3 //0000 1000

/** @brief Show the count through Leds in board
 * This function show the number of objetcs counted until the moment
 * @param[in] num Count
 * @return void
 */
void Mostrar_Leds(uint8_t num);
/** @brief Interruption function for switch 1
 * This function is the interruptcion function for switch 1.
 * Toggle a flag to change the status for the count system (ON//OFF).
 * @param[in] void
 * @return void
 */
void ISR_TEC1(void);
/** @brief Interruption function for switch 2
 * This function is the interruptcion function for switch 2.
 * Toggle a flag to change the status for the count system (HOLD ON//HOLD OFF).
 * @param[in] void
 * @return void
 */
void ISR_TEC2(void);
/** @brief Interruption function for switch 3
 * This function is the interruptcion function for switch 3.
 * Reset the count to 0.
 * @param[in] void
 * @return void
 */
void ISR_TEC3(void);
/** @brief Fuction to use with a timer
 * This function add a count and was make to use with a timer system.
 * @param[in] void
 * @return void
 */
void Contador(void);
/*==================[internal data definition]===============================*/

bool sw1 = false; //
bool sw2 = false; // banderas para decidir qué hacer en while(1)

uint8_t cuenta = 0; // contador
/*==================[internal functions declaration]=========================*/
void Mostrar_Leds(uint8_t num)
{
	/* Apago todos los leds */
	LedsOffAll();
	/* se pregunta por la situación de cada bit */
	if( (num&L1) != 0 ){
		LedOn(LED_1);
	}
	if( (num&L2) != 0){
		LedOn(LED_2);
	}
	if( (num&L3) != 0){
		LedOn(LED_3);
	}
	if( (num&Lrgb) != 0){
		LedOn(LED_RGB_B);
	}
}

void ISR_TEC1(void) // On/Off de conteo
{
	sw1 = !sw1;
}
void ISR_TEC2(void) // Hold
{
	sw2 = !sw2;
}
void ISR_TEC3(void) // Reseteo la cuenta
{
	cuenta = 0;
	Mostrar_Leds(cuenta);
}

void Contador(void) //función de cuenta para trabajar con Timer
{
	if(sw2 == false){ // el hold está inactivo, por lo que tengo que mostrar el valor del contador
		Mostrar_Leds(cuenta);
	}
}

/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){

	/*variables y banderas*/
	bool sensor;
	bool sensor_anterior;
	timer_config timer_init = {TIMER_A,1000,Contador};
	/*Inicializa todos los dispositivos y periféricos */
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	Tcrt5000Init(GPIO_T_COL0);
	TimerInit(&timer_init);
    /* Inicializa solicitudes de interrupciones*/
	SwitchActivInt(SWITCH_1, ISR_TEC1);
	SwitchActivInt(SWITCH_2, ISR_TEC2);
	SwitchActivInt(SWITCH_3, ISR_TEC3);
	/* Inicio el timer*/
	TimerStart(TIMER_A);

	while(1)
	{
		if(sw1 == true){ //se tiene que contar

			sensor = Tcrt5000State(); // mido el valor actual del sensor

			if(sensor==false && sensor_anterior==true){ //detecta flanco descendente, debo aumentar la cuenta
				if(cuenta < 15){ // como solo son 4 leds, puedo mostrar hasta 15 nada más
					cuenta ++;
				}
				else{
					cuenta = 0;
				}
			}
			sensor_anterior = sensor; //valor del sensor actual que será el anterior en la siguiente vuelta
		}
	}
    
	return 0;
}

/*==================[end of file]============================================*/


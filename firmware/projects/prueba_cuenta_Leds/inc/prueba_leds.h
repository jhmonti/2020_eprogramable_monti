/*! @mainpage Prueba MostrarLeds()
 *
 * \section genDesc General Description
 *
 * This application is a test for MostrarLeds() function which is utilized in contador objetos´s proyects
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 20/09/2020 | Document creation		                         |
 *
 * @author Joaquín H. Monti
 *
 */

#ifndef _TEMPLATE_H
#define _TEMPLATE_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */


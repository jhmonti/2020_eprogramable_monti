/*! @mainpage Prueba MostrarLeds()
 *
 * \section genDesc General Description
 *
 * This application is a test for MostrarLeds() function which is utilized in contador objetos´s proyects
 *

 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 22/09/2020 | Document creation		                         |
 *
 * @author Joaquín H. Monti
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/prueba_leds.h"       /* <= own header */

#include "systemclock.h"
#include "led.h"

/*==================[macros and definitions]=================================*/
#define COUNT_DELAY 30000000
#define L3  1<<0   //0000 0001
#define L2  1<<1   //0000 0010
#define L1  1<<2   //0000 0100
#define Lrgb  1<<3 //0000 1000
/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/
void Mostrar_Leds(uint8_t num)
{
	/* Apago todos los leds */
	LedOff(LED_1);
	LedOff(LED_2);
	LedOff(LED_3);
	LedOff(LED_RGB_B);
	/* se pregunta por la situación de cada bit */
	if( (num&L1) != 0 ){
		LedOn(LED_1);
	}
	if( (num&L2) != 0){
		LedOn(LED_2);
	}
	if( (num&L3) != 0){
		LedOn(LED_3);
	}
	if( (num&Lrgb) != 0){
		LedOn(LED_RGB_B);
	}
}

void Delay(void)
{
	uint32_t i;

	for(i=COUNT_DELAY; i!=0; i--)
	{
		   asm  ("nop");
	}
}
/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){
	uint8_t cont = 0;
	SystemClockInit();
	LedsInit();

    while(1){

    	if(cont < 15){
    		cont++;
    	}
    	else {
    		cont = 0;
    	}
    	Mostrar_Leds(cont);
    	Delay();
	}
    
	return 0;
}

/*==================[end of file]============================================*/


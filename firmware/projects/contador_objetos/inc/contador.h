/*! @mainpage Contador Objetos Interruptions
 *
 * \section genDesc General Description
 *
 * This application count objects with Tcrt5000 sensor and show it in natural binary code through Leds. \n
 * The next video describes how the program works.
 *
 * <a href="https://drive.google.com/file/d/1hqNT-hsQB2XcB40V7oUa5qalixc8n9h4/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   Tcrt5000  	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	 DOUT	 	|  GPIO_T_COL0	|
 * | 	 +5V	 	|  	  +5V		|
 * | 	 GND 	 	|  	  GND		|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 20/09/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Joaquín H. Monti
 *
 */

#ifndef _TEMPLATE_H
#define _TEMPLATE_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */


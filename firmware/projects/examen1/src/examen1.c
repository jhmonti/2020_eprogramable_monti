/*! @mainpage Parcial 02/11/2020
 *
 * \section genDesc Parcial de Electrónica Programable
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * |   Tcrt5000  	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	 DOUT	 	|  GPIO_T_COL0	|
 * | 	 +5V	 	|  	  +5V		|
 * | 	 GND 	 	|  	  GND		|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 02/11/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Joaquín H. Monti
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/examen1.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "bool.h"
#include "timer.h"
#include "uart.h"
#include "Tcrt5000.h"
/*==================[macros and definitions]=================================*/
#define BAUD_RATE 		115200 	/*!< BAUD_RATE: Velocidad de transmición de Datos en serie*/
#define PERIODO_TIMER  	1000 	/*!< PERIODO_TIMER: para que se transmita los datos cada 1[s]*/
#define RPM_40 			13		/*!< RPM_40: valor en [cuentas/seg] para comparar con las cuentas cada 1s*/
#define RPM_60			20		/*!< RPM_60: valor en [cuentas/seg] para comparar con las cuentas cada 1s*/
#define OK 				0
#define SLOW 			1
#define FAST			2
/*==================[internal data definition]===============================*/

uint32_t cuenta = 0; 	/*!< cuenta: cuenta de ranuras*/
uint8_t state;			/*!< state: estado de la rueda segun su velocidad: OK, SLOW, FAST*/

/*Banderas*/
bool mostrar_UART = false; 	/*!< mostrar_UART: bandera que sirve para mostrar en el while(1) cada 1[s]. Se activa en doTimer.*/

/*==================[internal functions declaration]=========================*/

/** @fn void doTimer(void);
 * @brief Function to use with a timer\n
 * This function add a count and was make to use with a timer system.
 * @param[in] void
 * @return void
 */
void doTimer(void);


/*==================[external data definition]===============================*/
/*variables para dispositivos y perifericos*/
timer_config timer_init = {TIMER_A, PERIODO_TIMER, doTimer}; 	/*!< Config para el timer*/
serial_config port = {SERIAL_PORT_PC,BAUD_RATE, NULL}; 			/*!< Config para el puerto de comunicación serie*/


/*==================[external functions definition]==========================*/
void hardwareConfig(void){

	/*Inicializa todos los dispositivos y periféricos */
	SystemClockInit();
	LedsInit();
	TimerInit(&timer_init);
	UartInit(&port);
	/*Apago Leds*/
	LedsOffAll();
	/* Inicio el timer*/
	TimerStart(TIMER_A);
}

void doTimer(void){
	/*cada 1s nos fijamos cuántas ranuras lleva contando, decidimos qué mostrar y reseteamos la cuenta*/

	if((cuenta>=RPM_40) && ((cuenta<=RPM_60))){
		state = OK;
	}
	if (cuenta<RPM_40){
		state = SLOW;
	}
	if (cuenta>RPM_60){
		state = FAST;
	}

	mostrar_UART = true;
	cuenta = 0;
}


int main(void){
	bool sensor;
	bool sensor_anterior;

	hardwareConfig();

    while(1){

		// ----------------MUESTRA POR UART Y PRENDER LEDS
		if(mostrar_UART){
		/* si es TRUE entonces es porque pasó 1s y tengo que encender leds y mandar por UART */

			switch (state){
				case OK:
					LedOn(LED_3);
					UartSendString(SERIAL_PORT_PC, "OK\r\n");
					break;
				case SLOW:
					LedOn(LED_1);
					UartSendString(SERIAL_PORT_PC, "SLOW\r\n");
				case FAST:
					LedOn(LED_2);
					UartSendString(SERIAL_PORT_PC, "FAST\r\n");
			}
			mostrar_UART = false; //para no encender y mandar en cada repeticion del while(1)
		}

		// ----------------LECTURA DEL SENSOR PARA CUENTA
		sensor = Tcrt5000State(); // mido el valor actual del sensor
		if(sensor==false && sensor_anterior==true){ //detecta flanco descendente, debo aumentar la cuenta
				cuenta ++;
		}
		sensor_anterior = sensor; //valor del sensor actual que será el anterior en la siguiente vuelta
	}
    

	return 0;
}

/*==================[end of file]============================================*/


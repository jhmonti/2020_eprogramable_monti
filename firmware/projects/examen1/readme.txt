﻿Este firmware forma parte de un sistema de medición de velocidad por infrerrojo, empleando el sensor TCRT5000.
Detecta el número de ranuras de una rueda cada 1seg y, en función de eso, enciende leds y envía un mensaje 
por UART. Las equivalencias con rpm son:
	1 rev = 20 cuentas
	1 min = 60 s
Cada 1s, realiza la comparación de la cantidad de cuentas con dos valores arbitrarios de velocidad en rpm:
	40 rpm = (40rpm * 20 cuentas)/60s = 13.3333 = 13 cuentas/s
	60 rpm = (60rpm * 20 cuentas)/60s = 20 cuentas/s
	
Entonces, se encienden leds y se manda mensaje por UART dependiendo si:
	La cuenta es menor a 13 => se enciende el LED 1 y se envía el mensaje "SLOW"
	La cuenta se encuentra entre 13 y 20 => se enciende el LED 3 y se envía el mensaje "OK"
	La cuenta es mayor a 20 => se enciende el LED 2 y se envía el mensaje "FAST"

Conecciones:
	
	TCRT5000	EDU_CIAA
		GND		GND
		VCC		+5V		
		DOUT	GPIO_T_COL0
	
	BAUD_RATE: 115200
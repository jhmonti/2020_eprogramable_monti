/*! @mainpage Parcial 02/11/2020
 *
 * \section genDesc Parcial de Electrónica Programable
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Sensor Presión |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	 DOUT	 	| 	 CH1		|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 02/11/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Joaquín H. Monti
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/examen2.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "Tcrt5000.h"
#include "bool.h"
#include "timer.h"
#include "uart.h"
#include "switch.h"

/*==================[macros and definitions]=================================*/
#define BAUD_RATE 		115200 	/*!< BAUD_RATE: Velocidad de transmición de Datos en serie*/
#define PERIODO_TIMER_A 40	 	/*!< PERIODO_TIMER_B: para tomar datos a una frecuencia de muestreo de 25Hz*/

#define MAX		1
#define MIN		2
#define PROM	3

/*==================[internal data definition]===============================*/
bool new_window = false;

float32_t max_calculado;
float32_t min_calculado;
float32_t prom_calculado;

uint8_t seleccion; //MAX,MIN o PROM
/*==================[internal functions declaration]=========================*/
/** @fn void hardwareConfig(void);
 * @brief Function to initialize hardware´s EDU-CIA\n
 * This function is used for initialize the hardware of microcontroller
 * @param[in] void
 * @return void
 */
void hardwareConfig(void);

/** @fn void doTimerA(void);
 * @brief Muestra los datos cada 1 seg
 * @param[in] void
 * @return void
 */
void doTimerA(void);

/** @fn void doADC(void);
 * @brief Function to use in analog-digital conversion\n
 * This function is used in conversion A-D of data
 * @param[in] void
 * @return void
 */
void doADC(void);

/** @brief Interruption function for switch 1
 * This function is the interruptcion function for switch 1.
 * Toggle a flag to show VALOR PROMEDIO.
 * @param[in] void
 * @return void
 */
void switch1(void);

/** @brief Interruption function for switch 2
 * This function is the interruptcion function for switch 2.
 * Toggle a flag to show VALOR MAXIMO.
 * @param[in] void
 * @return void
 */
void switch2(void);

/** @brief Interruption function for switch 3
 * This function is the interruptcion function for switch 3.
 * Toggle a flag to show VALOR MINIMO.
 * @param[in] void
 * @return void
 */
void switch3(void);

/*==================[external data definition]===============================*/
/*variables para los drivers del micro*/
timer_config timer_initA = {TIMER_A, PERIODO_TIMER_A, doTimerA};/*!< Config para el timer */
serial_config port = {SERIAL_PORT_PC,BAUD_RATE, NULL}; 			/*!< Config para el puerto de comunicación serie*/
analog_input_config ADC = {CH1, AINPUTS_SINGLE_READ, doADC}; 	/*!< Config para el Conversor Analógico-Digital*/


/*==================[external functions definition]==========================*/
void hardwareConfig(void){
	/*Apago los leds porque me molestan que esten prendidos*/
	LedsInit();
	LedsOffAll();
	/*Inicializa todos los dispositivos y periféricos */
	SystemClockInit();
	SwitchesInit();
	TimerInit(&timer_initA);
	UartInit(&port);
	AnalogInputInit(&ADC);
	AnalogOutputInit();
    /* Inicializa solicitudes de interrupciones*/
	SwitchActivInt(SWITCH_1, switch1);
	SwitchActivInt(SWITCH_2, switch2);
	SwitchActivInt(SWITCH_3, switch3);
	/* Inicio el timer*/
	TimerStart(TIMER_A);
}

void doADC(void){

	static uint8_t i_muestras = 0;
	uint16_t dato;
	static float32_t max = 0;
	static float32_t min = 200;
	static float32_t acum;
	float32_t presion;
	AnalogInputRead(CH1,&dato); //leemos el valor
	presion = dato*(200/1024); //lo transformamos
	if(i_muestras < 25){
		if(max < presion){
			max = presion;
		}
		if(min > presion){
			min = presion;
		}
		acum += presion;
		i_muestras++;
	}
	else {
		max_calculado = max;
		min_calculado = min;
		prom_calculado = acum/(float)25;
		new_window = true;
		min = presion;
		max = presion;
		acum = presion;
		i_muestras = 0;
	}
}

void doTimerA(void){
	AnalogStartConvertion();
}

void switch1(void){
	seleccion = PROM;
}

void switch2(void){
	seleccion = MAX;
}

void switch3(void){
	seleccion = MIN;
}

int main(void){

    while(1){
		if(new_window){
			switch (seleccion){
				case MAX:
					UartSendString(SERIAL_PORT_PC, UartItoa(max_calculado, 10));
					break;
				case MIN:
					UartSendString(SERIAL_PORT_PC, UartItoa(min_calculado, 10));
					break;
				case PROM:
					UartSendString(SERIAL_PORT_PC, UartItoa(prom_calculado, 10));
					break;
			new_window = false;
			}

		}
	}
    
	return 0;
}

/*==================[end of file]============================================*/


/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Display 3digit	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	 D_A	 	| 	GPIO_LCD_1	|
 * | 	 D_B	 	| 	GPIO_LCD_2	|
 * | 	 D_C	 	| 	GPIO_LCD_3	|
 * | 	 D_D	 	| 	GPIO_LCD_4	|
 * | 	  U 	 	| 	GPIO_5		|
 * | 	  D 	 	| 	GPIO_3		|
 * | 	  C 	 	| 	GPIO_1		|
 * | 	 Vdd	 	| 	+5V 		|
 * | 	 Vss	 	| 	GND 		|
 *
 * |   Tcrt5000  	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	 DOUT	 	|  GPIO_T_COL0	|
 * | 	 +5V	 	|  	  +5V		|
 * | 	 GND 	 	|  	  GND		|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/11/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Albano Peñalva
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/prueba_display_7_3.h"       /* <= own header */
#include "display_7seg_3dig.h"
#include "Tcrt5000.h"
#include "systemclock.h"
#include "led.h"
/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/

uint16_t cuenta = 0; // contador
/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){
	/*variables y banderas*/
	bool sensor;
	bool sensor_anterior;
	/*Inicializa todos los dispositivos y periféricos */
	SystemClockInit();
	LedsInit();
	LedsOffAll();
	Tcrt5000Init(GPIO_T_COL0);
	DisplayInit();

    while(1){

    	sensor = Tcrt5000State(); // mido el valor actual del sensor
		if(sensor==false && sensor_anterior==true){ //detecta flanco descendente, debo aumentar la cuenta
			if(cuenta < 15){ // como solo son 4 leds, puedo mostrar hasta 15 nada más
				cuenta ++;
			}
			else{
				cuenta = 0;
			}
		}
		sensor_anterior = sensor; //valor del sensor actual que será el anterior en la siguiente vuelta

		if(cuenta < 999){
			LcdItsE0803Write(cuenta);
		}
		else{
			cuenta = 0;
		}

	}
    
	return 0;
}

/*==================[end of file]============================================*/


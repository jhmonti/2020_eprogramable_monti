/*! @mainpage Osciloscopio Digital
 *
 * \section genDesc General Description
 *
 * This application show a ecg signal in "Serial Osciloscope" program.\n
 *  Use DAC for convert a data of signal to analog data, \n
 *  and ADC for convert this analog data to digital data. Finally, \n
 *  it sends them through serial port to the "Serial Osciloscope" program. \n
 *
 * \section hardConn Hardware Connection
 *
 * Connect CH1 and DAC through a potentiometer
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 09/10/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Joaquín H. Monti
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/osciloscopio.h"       /* <= own header */

#include "systemclock.h"
#include "led.h"
#include "bool.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"
/*==================[macros and definitions]=================================*/
#define BAUD_RATE   115200 	/*!< BAUD_RATE: Velocidad de transmición de Datos en serie*/
#define BUFFER_SIZE 231		/*!< BUFFER_SIZE: Cantidad de datos en la señal de ECG*/

const char ecg[BUFFER_SIZE]={ /*!< señal de ECG*/
		76, 77, 78, 77, 79, 86, 81, 76, 84, 93, 85, 80,
		89, 95, 89, 85, 93, 98, 94, 88, 98, 105, 96, 91,
		99, 105, 101, 96, 102, 106, 101, 96, 100, 107, 101,
		94, 100, 104, 100, 91, 99, 103, 98, 91, 96, 105, 95,
		88, 95, 100, 94, 85, 93, 99, 92, 84, 91, 96, 87, 80,
		83, 92, 86, 78, 84, 89, 79, 73, 81, 83, 78, 70, 80, 82,
		79, 69, 80, 82, 81, 70, 75, 81, 77, 74, 79, 83, 82, 72,
		80, 87, 79, 76, 85, 95, 87, 81, 88, 93, 88, 84, 87, 94,
		86, 82, 85, 94, 85, 82, 85, 95, 86, 83, 92, 99, 91, 88,
		94, 98, 95, 90, 97, 105, 104, 94, 98, 114, 117, 124, 144,
		180, 210, 236, 253, 227, 171, 99, 49, 34, 29, 43, 69, 89,
		89, 90, 98, 107, 104, 98, 104, 110, 102, 98, 103, 111, 101,
		94, 103, 108, 102, 95, 97, 106, 100, 92, 101, 103, 100, 94, 98,
		103, 96, 90, 98, 103, 97, 90, 99, 104, 95, 90, 99, 104, 100, 93,
		100, 106, 101, 93, 101, 105, 103, 96, 105, 112, 105, 99, 103, 108,
		99, 96, 102, 106, 99, 90, 92, 100, 87, 80, 82, 88, 77, 69, 75, 79,
		74, 67, 71, 78, 72, 67, 73, 81, 77, 71, 75, 84, 79, 77, 77, 76, 76,
};

/*==================[internal data definition]===============================*/
uint16_t valorADC; 	/*!< valorADC: Varable para guardar el valor convertido A-D*/
uint8_t muestra = 0;/*!< muestra: Índice para recorrer el vector de ecg y mandar los datos al osciloscopio*/



/*==================[internal functions declaration]=========================*/



/*==================[external data definition]===============================*/
/*variables para los drivers del micro*/
timer_config timer_initA = {TIMER_A, 2, doTimerA}; 				/*!< Config para el timer de conversión AD*/
timer_config timer_initB = {TIMER_B, 4, doTimerB}; 				/*!< Config para el timer de conversión DA*/
serial_config port = {SERIAL_PORT_PC,BAUD_RATE, NULL}; 			/*!< Config para el puerto de comunicación serie*/
analog_input_config ADC = {CH1, AINPUTS_SINGLE_READ, doADC}; 	/*!< Config para el Conversor Analógico-Digital*/

/*==================[external functions definition]==========================*/
void hardwareConfig(void){
	/*Apago los leds porque me molestan que esten prendidos*/
	LedsInit();
	LedsOffAll();
	/*Inicializa todos los dispositivos y periféricos */
	SystemClockInit();
	TimerInit(&timer_initA);
	TimerInit(&timer_initB);
	UartInit(&port);
	AnalogInputInit(&ADC);
	AnalogOutputInit();
	/* Inicio el timer*/
	TimerStart(TIMER_A);
	TimerStart(TIMER_B);
}

void doTimerA(void){
	AnalogStartConvertion();
}

void doTimerB(void){
	if (muestra<BUFFER_SIZE){ //Si es menor al tamaño del vector
		AnalogOutputWrite(ecg[muestra]);
		muestra++;
	}
	else{ //Sino, lo reseteo
		muestra = 0;
	}
}

void doADC(void){
	AnalogInputRead(CH1,&valorADC); //leemos el valor convertido
	//enviamos el dato en código ASCII por puerto serie al programa del osciloscopio
	UartSendString(SERIAL_PORT_PC, UartItoa(valorADC, 10));
	UartSendString(SERIAL_PORT_PC, "\r");
}


/*------------------------------------------------------------------------------*/
int main(void){

	hardwareConfig();
    while(1){

	}
    
	return 0;
}

/*==================[end of file]============================================*/


/*! @mainpage Osciloscopio Digital
 *
 * \section genDesc General Description
 *
  * This application show a ecg signal in "Serial Osciloscope" program.\n
 *  Use DAC for convert a data of signal to analog data, \n
 *  and ADC for convert this analog data to digital data. Finally, \n
 *  it sends them through serial port to the "Serial Osciloscope" program. \n
 * The next video describes how the program works.
 *
 * <a href=" AGREGAR ">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * Connect CH1 and DAC through a potentiometer
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 09/10/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Joaquín H. Monti
 *
 */

#ifndef _TEMPLATE_H
#define _TEMPLATE_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

/** @fn int main(void);
 * @brief Main function\n
 * @param[in] void
 * @return 0 if it was ok
 */
int main(void);
/** @fn void hardwareConfig(void);
 * @brief Function to initialize hardware´s EDU-CIA\n
 * This function is used for initialize the hardware of microcontroller
 * @param[in] void
 * @return void
 */
void hardwareConfig(void);

/** @fn void doTimerA(void);
 * @brief This function start the data conversion (analog-digital) with a frecuency of 500Hz (period = 2ms)
 * @param[in] void
 * @return void
 */
void doTimerA(void);

/** @fn void doTimerB(void);
 * @brief This function start the data conversion (digital-analog) with a frecuency of 250Hz (period = 4ms)
 * @param[in] void
 * @return void
 */
void doTimerB(void);

/** @fn void doSerial(void);
 * @brief Function to use in serial communication\n
 * This function is used in transmission of data
 * @param[in] void
 * @return void
 */
void doSerial(void);

/** @fn void doADC(void);
 * @brief Function to use in analog-digital conversion\n
 * This function is used in conversion A-D of data
 * @param[in] void
 * @return void
 */
void doADC(void);
/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */


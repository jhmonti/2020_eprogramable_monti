﻿Este proyecto es un sistema de monitoreo de frecuencia cardíaca.
Toma la señal de ecg, la transforma en digital, realiza un filtrado pasa-bajos y detecta las ondas R.
Con la detección de las ondas R sucesivas, realiza el cálculo de la frecuencia y lo muestra por el display
7segmentos 3dígitos.

Video de Funcionamiento: https://youtu.be/7as2kSWDVdE

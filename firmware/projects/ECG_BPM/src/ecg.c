/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Albano Peñalva
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/ecg.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "bool.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"
#include "switch.h"
#include "display_7seg_3dig.h"
#include <math.h>
#include "fpu_init.h"

/*==================[macros and definitions]=================================*/
#define BAUD_RATE   460800 	/*!< BAUD_RATE: Velocidad de transmición de Datos en serie*/
#define BUFFER_SIZE 231		/*!< BUFFER_SIZE: Cantidad de datos en la señal de ECG*/
#define FREC_40HZ	40		/*!< FREC_40HZ: Frecuencia de corte para el filtro pasa-bajos, se propone 40Hz para monitoreo*/
#define pi 3.141592			/*!< pi: Valor de el num. pi*/

const char ecg[BUFFER_SIZE]={ /*!< señal de ECG*/
		76, 77, 78, 77, 79, 86, 81, 76, 84, 93, 85, 80,
		89, 95, 89, 85, 93, 98, 94, 88, 98, 105, 96, 91,
		99, 105, 101, 96, 102, 106, 101, 96, 100, 107, 101,
		94, 100, 104, 100, 91, 99, 103, 98, 91, 96, 105, 95,
		88, 95, 100, 94, 85, 93, 99, 92, 84, 91, 96, 87, 80,
		83, 92, 86, 78, 84, 89, 79, 73, 81, 83, 78, 70, 80, 82,
		79, 69, 80, 82, 81, 70, 75, 81, 77, 74, 79, 83, 82, 72,
		80, 87, 79, 76, 85, 95, 87, 81, 88, 93, 88, 84, 87, 94,
		86, 82, 85, 94, 85, 82, 85, 95, 86, 83, 92, 99, 91, 88,
		94, 98, 95, 90, 97, 105, 104, 94, 98, 114, 117, 124, 144,
		180, 210, 236, 253, 227, 171, 99, 49, 34, 29, 43, 69, 89,
		89, 90, 98, 107, 104, 98, 104, 110, 102, 98, 103, 111, 101,
		94, 103, 108, 102, 95, 97, 106, 100, 92, 101, 103, 100, 94, 98,
		103, 96, 90, 98, 103, 97, 90, 99, 104, 95, 90, 99, 104, 100, 93,
		100, 106, 101, 93, 101, 105, 103, 96, 105, 112, 105, 99, 103, 108,
		99, 96, 102, 106, 99, 90, 92, 100, 87, 80, 82, 88, 77, 69, 75, 79,
		74, 67, 71, 78, 72, 67, 73, 81, 77, 71, 75, 84, 79, 77, 77, 76, 76,
};

/*==================[internal data definition]===============================*/
uint16_t valorADC; 				/*!< valorADC: Variable para guardar el valor convertido A-D*/
uint8_t muestra = 0;			/*!< muestra: Índice para recorrer el vector de ecg y mandar los datos al osciloscopio*/
float frec_cardiaca = 0; 		/*!< frec_cardiaca: Valor de la frecuencia cardíaca */
uint16_t periodo_timer = 4000;	/*!< periodo_timer: Periodo en ms del timer para mandar los datos D-A*/
uint16_t valor_filtrado;
uint16_t valor_anterior = 0;
uint32_t cont_muestra = 0;
uint32_t cont_muestra_anterior = 0;
uint16_t umbral = 190;

//  Variables del filtro
uint16_t fs = 500;				/*!< fs: Frecuencia de muestreo*/
uint16_t senial_anterior = 0;	/*!< senial_anterior: Valor de la senial filtrada anterior (BUFFER) */


//	Banderas
bool sw1 = false; 		/*!< sw1: Bandera para saber si se aumento la frecuencia de los datos D-A*/
bool sw2 = false;		/*!< sw2: Bandera para saber si se disminuyo la frecuencia de los datos D-A*/
bool QRS = false;		/*!< QRS: Bandera para saber si se detecto un complejo QRS (supera el umbral)*/
/*==================[internal functions declaration]=========================*/

/** @fn void hardwareConfig(void);
 * @brief Function to initialize hardware´s EDU-CIA\n
 * This function is used for initialize the hardware of microcontroller
 * @param[in] void
 * @return void
 */
void hardwareConfig(void);

/** @fn void doTimerA(void);
 * @brief This function start the data conversion (analog-digital) with a frecuency of 500Hz (period = 2ms)
 * @param[in] void
 * @return void
 */
void doTimerB(void);

/** @fn void doTimerB(void);
 * @brief This function start the data conversion (digital-analog) with a frecuency of 250Hz (period = 4ms)
 * @param[in] void
 * @return void
 */
void doTimerC(void);

/** @fn void doSerial(void);
 * @brief Function to use in serial communication\n
 * This function is used in transmission of data
 * @param[in] void
 * @return void
 */
void doSerial(void);

/** @fn void doADC(void);
 * @brief Function to use in analog-digital conversion\n
 * This function is used in conversion A-D of data
 * @param[in] void
 * @return void
 */
void doADC(void);

/** @brief Interruption function for switch 1
 * This function is the interruptcion function for switch 1.
 * Toggle a flag to set on the filter(ON).
 * @param[in] void
 * @return void
 */
void switch1(void);

/** @brief Interruption function for switch 2
 * This function is the interruptcion function for switch 2.
 * Toggle a flag to set off the filter(OFF).
 * @param[in] void
 * @return void
 */
void switch2(void);

/** @brief Interruption function for switch 3
 * This function is the interruptcion function for switch 3.
 * Decreases the corner frecuency of the filter.
 * @param[in] void
 * @return void
 */
void switch3(void);

/** @brief Interruption function for switch 4
 * This function is the interruptcion function for switch 3.
 * increases the corner frecuency of the filter.
 * @param[in] void
 * @return void
 */
void switch4(void);

/** @brief Low-pass filter algorithm
 * This function implements the next Low-pass filter algorithm:
 *
 * salida_filtrada[i] = salida_filtrada[i-1]+α*(entrada[i]-salida_filtrada[i-1])
 *
 * where:
 * 		α = dt / (RC + dt)
 * and
 * 		RC = 1 / (2*pi*fc)
 * @param[in] senial Input signal
 * @param[in] fc Corner frequency
 *
 * @return senial_filtrada Filtered signal
 */
uint16_t LowPassFilt(uint16_t senial, uint8_t fc);

/** @brief Calculation of the constant alpha
 * This function calculates the constant alpha of the filter.
 * Is used for LowPassFilt().
 * @param[in] fc Corner frequency
 * @return alfa Constant alfa
 */
float calculoAlfa(uint8_t fc);


/*==================[external data definition]===============================*/
/*variables para los drivers del micro*/
timer_config timer_initB = {TIMER_B, 2, doTimerB}; 				/*!< Config para el timer de conversión AD*/
timer_config timer_initC = {TIMER_C, 4000, doTimerC}; 			/*!< Config para el timer de conversión DA*/
serial_config port = {SERIAL_PORT_PC,BAUD_RATE, NULL}; 			/*!< Config para el puerto de comunicación serie*/
analog_input_config ADC = {CH1, AINPUTS_SINGLE_READ, doADC}; 	/*!< Config para el Conversor Analógico-Digital*/

/*==================[external functions definition]==========================*/
void hardwareConfig(void){
	/*Apago los leds al comienzo*/
	LedsInit();
	LedsOffAll();
	/*Inicializa todos los dispositivos y periféricos */
	SystemClockInit();
	SwitchesInit();
	TimerInit(&timer_initC);
	TimerInit(&timer_initB);
	UartInit(&port);
	AnalogInputInit(&ADC);
	AnalogOutputInit();
	DisplayInit();
    /* Inicializa solicitudes de interrupciones*/
	SwitchActivInt(SWITCH_1, switch1);
	SwitchActivInt(SWITCH_2, switch2);
	SwitchActivInt(SWITCH_3, switch3);
	SwitchActivInt(SWITCH_4, switch4);
	/* Inicio el timer*/
	TimerStart(TIMER_C);
	TimerStart(TIMER_B);
}

void doTimerB(void){
	/*toma el valor del puerto analogico y lo transforma en digital*/
	AnalogStartConvertion();
}

void doTimerC(void){
	/*envia los valores del vector ecg, transformandolos de digital a analogico*/
	if (muestra<BUFFER_SIZE){ //Si es menor al tamaño del vector
		AnalogOutputWrite(ecg[muestra]);
		muestra++;
	}
	else{ //Sino, lo reseteo
		muestra = 0;
	}
}

void doADC(void){

	cont_muestra++;

	AnalogInputRead(CH1,&valorADC); //leemos el valor convertido
	valor_filtrado = LowPassFilt(valorADC,FREC_40HZ); //filtramos con el pasa-bajos

	/*Mostramos en el osciloscopio la senial y el umbral utilizado para el calculo*/
	UartSendString(SERIAL_PORT_PC, UartItoa(valor_filtrado, 10));
	UartSendString(SERIAL_PORT_PC, ",");
	UartSendString(SERIAL_PORT_PC, UartItoa(umbral, 10));
	UartSendString(SERIAL_PORT_PC, "\r");

	//DETECCION QRS
	double T;
	if ((cont_muestra-cont_muestra_anterior) > (double)50){ //tiempo refractario -> no se busca otro R hasta que pasen 50muestras.

		if((valor_filtrado > umbral) && (valor_filtrado > valor_anterior)){
			T = fabs((double)(cont_muestra-cont_muestra_anterior)) / fs; 	/*periodo entre ondas R*/
			frec_cardiaca = (float)60/(float)T;  							/*Frecuencia cardiaca en LPM*/

			cont_muestra_anterior = cont_muestra;
		}
	}

	valor_anterior = valor_filtrado;
}

void switch1(void){
	if(periodo_timer<25000){ /*aumenta el periodo solo si es menor a 25000 (25000us -> 1000Hz)*/
		periodo_timer= periodo_timer+100;
	}
	else{
		periodo_timer = 4000; /*reinicia el periodo*/
	}
	/*Se resetea el timer C con el nuevo valor de periodo*/
	TimerStop(TIMER_C);
	timer_initC.period = periodo_timer;
	TimerInit(&timer_initC);
	TimerStart(TIMER_C);
}

void switch2(void){
	if(periodo_timer>1000){ /*aumenta el periodo solo si es mayor a 1000 (1000us -> 40Hz)*/
		periodo_timer=periodo_timer-100;
	}
	else{
		periodo_timer = 4000; /*reinicia el periodo*/
	}
	/*Se resetea el timer C con el nuevo valor de periodo*/
	TimerStop(TIMER_C);
	timer_initC.period = periodo_timer;
	TimerInit(&timer_initC);
	TimerStart(TIMER_C);
}

void switch3(void){
// Disminuye Umbral
	umbral--;
}

void switch4(void){
// Aumenta umbral
	umbral++;
}

float calculoAlfa(uint8_t fc){
	/* 		α = dt / (RC + dt)
	* and
	* 		RC = 1 / (2*pi*fc)
	*/
	float dt =  1/((float)fs);
	float alfa = dt / ((1/(2*pi*fc)) + dt);
	return alfa;


}

uint16_t LowPassFilt(uint16_t senial, uint8_t fc){
	/*	salida_filtrada[i] = salida_filtrada[i-1] + α*(entrada[i]-salida_filtrada[i-1])
	*  where:
	* 		α = dt / (RC + dt)
	* and
	* 		RC = 1 / (2*pi*fc)
	*/
	float alfa = calculoAlfa(fc);
	uint16_t senial_filtrada = senial_anterior + alfa*(senial-senial_anterior);
	senial_anterior = senial_filtrada;

	return senial_filtrada;
}



/*------------------------------------------------------------------------------*/
int main(void){
	hardwareConfig();
	fpuInit();

    while(1){

    	DisplayWrite(ceil(frec_cardiaca));
	}
    
	return 0;
}
/*==================[end of file]============================================*/


/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>

/*==================[macros and definitions]=================================*/
typedef enum{
	ON,
	OFF,
	TOGGLE,
}state_t;

typedef enum{
	led_1=1,
	led_2,
	led_3,
}led_t;

typedef struct
{
    uint8_t n_led;      //indica el número de led a controlar
    uint8_t n_ciclos;   //indica la cantidad de ciclos de encendido/apagado
    uint8_t periodo;    //indica el tiempo de cada ciclo
    uint8_t mode;       //ON, OFF, TOGGLE
} leds;

void LedControl(leds *l);

/*==================[internal functions declaration]=========================*/
void LedControl(leds *l){

	uint8_t i=0;
	uint8_t j=0;

	switch(l->mode){
	case ON:
		switch(l->n_led){
		case led_1:
			printf("se enciende : %d", led_1);
			break;
		case led_2:
			printf("se enciende : %d", led_2);
			break;
		case led_3:
			printf("se enciende : %d", led_3);
			break;
		}
		break;
	case OFF:
		switch(l->n_led){
		case led_1:
			printf("se apaga : %d", led_1);
			break;
		case led_2:
			printf("se apaga : %d", led_2);
			break;
		case led_3:
			printf("se apaga : %d", led_3);
		break;
		}
		break;
	case TOGGLE:
		for(i=0; i<l->n_ciclos;i++){
			switch(l->n_led){
				case led_1:
					printf("se togglea : %d", led_1);
					break;
				case led_2:
					printf("se togglea : %d", led_2);
					break;
				case led_3:
					printf("se togglea : %d", led_3);
					break;
				}
			for(j=0; j<l->periodo;j++){}//espera pasiva de retardo
		}
		break;
	}
}
int main(void)
{
	leds my_leds={led_1,10,200,TOGGLE};
	LedControl(&my_leds);
	return 0;
}

/*==================[end of file]============================================*/


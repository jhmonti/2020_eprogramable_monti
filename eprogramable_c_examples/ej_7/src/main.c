/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>

/*==================[macros and definitions]=================================*/
#define BIT3 3
#define BIT13 13
#define BIT14 14
uint32_t mask_1 = 1 << BIT3;
uint32_t mask_0_13 = 1 << BIT13;
uint32_t mask_0_14 = 1 << BIT14;

uint32_t Seteo(uint32_t num);
/*==================[internal functions declaration]=========================*/
uint32_t Seteo(uint32_t num){

	uint32_t numr = 0;
	mask_0_13 = ~mask_0_13;
	mask_0_14 = ~mask_0_14;
	numr = num & mask_0_13; //pongo el bit 13 en 0
	numr &= mask_0_14; //pongo el bit 14 en 0
	numr |= mask_1;	   //pongo el bit 3 en 1
	return numr;
}

int main(void)
{
    uint32_t num = 32;
    printf("El numero es : %d", num);
    uint32_t num_seteo = Seteo(num);
    printf("El numero despues del seteo es: %d", num_seteo);

    return 0;
}

/*==================[end of file]============================================*/


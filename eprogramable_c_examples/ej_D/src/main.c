/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JM Reta - jmreta@ingenieria.uner.edu.ar
 * Eduardo Filomena
 * Gonzalo Cuenca
 * Juan Ignacio Cerrudo
 * Albano Peñalva
 * Sebastián Mateos
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 *
 C) Escriba una función que reciba un dato de 32 bits,  la cantidad de dígitos de salida y un puntero a un arreglo donde se almacene los n dígitos. La función deberá convertir el dato recibido a BCD, guardando cada uno de los dígitos de salida en el arreglo pasado como puntero.

int8_t  BinaryToBcd (uint32_t data, uint8_t digits, uint8_t * bcd_number )
{

}

 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>

/*==================[macros and definitions]=================================*/
typedef struct
{
    uint8_t port;                /*!< GPIO port number */
    uint8_t pin;                /*!< GPIO pin number */
    uint8_t dir;                /*!< GPIO direction ‘0’ IN;  ‘1’ OUT */
} gpioConf_t;

void  BinaryToBcd (uint32_t data, uint8_t digits, uint8_t * bcd_number );
void printBits(size_t const size, void const * const ptr);
uint8_t BcdToLcd(uint8_t digito,gpioConf_t * config);
/*==================[internal functions declaration]=========================*/

void printBits(size_t const size, void const * const ptr)
{
    unsigned char *b = (unsigned char*) ptr;
    unsigned char byte;
    int i, j;

    for (i=size-1;i>=0;i--)
    {
        for (j=7;j>=0;j--)
        {
            byte = (b[i] >> j) & 1;
            printf("%u", byte);
        }
    }
    puts("");
}

uint8_t BcdToLcd(uint8_t digito,gpioConf_t * config)
{
	printBits(sizeof(digito),&digito);
	uint8_t i,aux;
	for(i=0;i<4;i++){
		aux = pow(2,i);
		if(digito & aux){
			printf("\r\nSe pone a 1 el pin %d del puerto %d", config[i].pin,config[i].port);
		}
		else{
			printf("\r\nSe pone a 0 el pin %d del puerto %d", config[i].pin,config[i].port);
		}
	}
	return 0;
}




void  BinaryToBcd (uint32_t data, uint8_t digits, uint8_t * bcd_number )
{
	while(digits > 0){
		digits--;
		printf("digito: %d  \r\n", digits);
		bcd_number[digits] = data%10;
		printf("digito: %d \r\n  ", bcd_number[digits]);
		data=data/10;
	}
}
int main(void)
{
	gpioConf_t conf[] = {{1,4,1},{1,5,1},{1,6,1},{1,7,1}};
	BcdToLcd(4,conf);

	return 0;
}

/*==================[end of file]============================================*/


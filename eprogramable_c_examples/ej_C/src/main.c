/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JM Reta - jmreta@ingenieria.uner.edu.ar
 * Eduardo Filomena
 * Gonzalo Cuenca
 * Juan Ignacio Cerrudo
 * Albano Peñalva
 * Sebastián Mateos
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 *
 C) Escriba una función que reciba un dato de 32 bits,  la cantidad de dígitos de salida y un puntero a un arreglo donde se almacene los n dígitos. La función deberá convertir el dato recibido a BCD, guardando cada uno de los dígitos de salida en el arreglo pasado como puntero.

int8_t  BinaryToBcd (uint32_t data, uint8_t digits, uint8_t * bcd_number )
{

}

 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>

/*==================[macros and definitions]=================================*/
void  BinaryToBcd (uint32_t data, uint8_t digits, uint8_t * bcd_number );
/*==================[internal functions declaration]=========================*/
void  BinaryToBcd (uint32_t data, uint8_t digits, uint8_t * bcd_number )
{
	while(digits > 0){
		digits--;
		printf("digito: %d  \r\n", digits);
		bcd_number[digits] = data%10;
		printf("digito: %d \r\n  ", bcd_number[digits]);
		data=data/10;
	}
}
int main(void)
{
	 uint32_t dato = 1234;
	 uint8_t numeros[4];
	 BinaryToBcd(dato, 4, numeros);
	 printf("digito: %d: \r\n",numeros[2]);

	return 0;
}

/*==================[end of file]============================================*/


/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es: Joaquín H. Monti
 * JM Reta - jmreta@ingenieria.uner.edu.ar
 * Eduardo Filomena
 * Gonzalo Cuenca
 * Juan Ignacio Cerrudo
 * Albano Peñalva
 * Sebastián Mateos
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>


/*==================[macros and definitions]=================================*/

void printBits(size_t const size, void const * const ptr);
/*==================[internal functions declaration]=========================*/

void printBits(size_t const size, void const * const ptr)
{
    unsigned char *b = (unsigned char*) ptr;
    unsigned char byte;
    int i, j;

    for (i=size-1;i>=0;i--)
    {
        for (j=7;j>=0;j--)
        {
            byte = (b[i] >> j) & 1;
            printf("%u", byte);
        }
    }
    puts("");
}

int main(void)
{

	uint32_t num = 0x01020304;
    printBits(sizeof(num), &num);

    int i;
    uint32_t mask;
    uint8_t bcd1;
    uint8_t bcd2;
    uint8_t bcd3;
    uint8_t bcd4;
    // Para sacar los bytes de la variable 32b, podemos hacerlo mediante & con pow(2,i):
    bcd1 = 0;
    for (i=0; i<8; i++){
    	mask = pow(2,i); //i sería la posición del byte que quiero extraer
    	printf("mask:");
    	printBits(sizeof(mask),&mask);
    	bcd1 += num&mask;
    }
    printBits(sizeof(bcd1),&bcd1);
    //
    bcd2 = 0;
    for (i=8; i<16; i++){
       	mask = pow(2,i); //i sería la posición del byte que quiero extraer
       	printf("mask:");
       	printBits(sizeof(mask),&mask);
       	bcd2 += (num&mask)>>8;
    }
    printBits(sizeof(bcd2),&bcd2);
    //
    bcd3 = 0;
    for (i=16; i<24; i++){
       	mask = pow(2,i); //i sería la posición del byte que quiero extraer
       	printf("mask:");
       	printBits(sizeof(mask),&mask);
       	bcd3 += (num&mask)>>16;
    }
    printBits(sizeof(bcd3),&bcd3);
    //
    bcd4 = 0;
    for (i=24; i<32; i++){
    	mask = pow(2,i); //i sería la posición del byte que quiero extraer
    	printf("mask:");
    	printBits(sizeof(mask),&mask);
    	bcd4 += (num&mask)>>24;
    }
    printBits(sizeof(bcd4),&bcd4);

    return 0;

}

/*==================[end of file]============================================*/


########################################################################
#Cátedra: Electrónica Programable
#FIUNER
#2018
#Autor/es:
#JMreta - jmreta@ingenieria.uner.edu.ar
#
#Revisión:
########################################################################

# Ruta del Proyecto
####Ejemplo 1: Hola Mundo
##Por el primero, se debe usar Makefile1
#PROYECTO_ACTIVO = 1_hola_mundo
#NOMBRE_EJECUTABLE = hola_mundo.exe

####Ejemplo 2: Control Mundo
#PROYECTO_ACTIVO = 2_control_mundo
#NOMBRE_EJECUTABLE = control_mundo.exe

####Ejemplo 3: Promedio
#PROYECTO_ACTIVO = 3_promedio_1
#NOMBRE_EJECUTABLE = promedio.exe

#PROYECTO_ACTIVO = 5_menu
#NOMBRE_EJECUTABLE = menu.exe

#PROYECTO_ACTIVO = Ej_c_d_e
#NOMBRE_EJECUTABLE = c_d_e.exe

####Ejercicio 7:
#PROYECTO_ACTIVO = ej_7
#NOMBRE_EJECUTABLE = ej_7.exe

####Ejercicio 9:
#PROYECTO_ACTIVO = ej_9
#NOMBRE_EJECUTABLE = ej_9.exe

####Ejercicio 12:
#PROYECTO_ACTIVO = ej_12
#NOMBRE_EJECUTABLE = ej_12.exe

####Ejercicio 14:
#PROYECTO_ACTIVO = ej_14
#NOMBRE_EJECUTABLE = ej_14.exe

####Ejercicio 16:
#PROYECTO_ACTIVO = ej_16
#NOMBRE_EJECUTABLE = ej_16.exe

####Ejercicio Integrador A:
#PROYECTO_ACTIVO = ej_A
#NOMBRE_EJECUTABLE = ej_A.exe

####Ejercicio Integrador C:
#PROYECTO_ACTIVO = ej_C
#NOMBRE_EJECUTABLE = ej_C.exe

####Ejercicio Integrador CD:
PROYECTO_ACTIVO = ej_D
NOMBRE_EJECUTABLE = ej_D.exe
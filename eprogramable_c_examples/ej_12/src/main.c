/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2020
 * Autor/es: Joaquín H. Monti

 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>
/*==================[macros and definitions]=================================*/
#define BIT4 3
uint16_t mask = 1 << BIT4;

void printBits(size_t const size, void const * const ptr);
/*==================[internal functions declaration]=========================*/

void printBits(size_t const size, void const * const ptr)
{
    unsigned char *b = (unsigned char*) ptr;
    unsigned char byte;
    int i, j;

    for (i=size-1;i>=0;i--)
    {
        for (j=7;j>=0;j--)
        {
            byte = (b[i] >> j) & 1;
            printf("%u", byte);
        }
    }
    puts("");
}

int main(void)
{
    int16_t num; //entero con signo de 16bit
    int16_t *pnum;// puntero
    pnum = &num;   //el puntero va a apuntar a num
    num = -1;
    printf("El numero es : %d \r\n", num);
    printBits(sizeof(num), &num);

    mask = ~mask;
    *pnum &= mask;

    printf("El numero despues del seteo es: %d \r\n", num);
    printBits(sizeof(num), &num);

    return 0;
}

/*==================[end of file]============================================*/

